﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D body;
    private SpriteRenderer rend;
    private Animator anim; 
    private float h;
    private bool jump = false;
    public bool isGrounded = true;
    private float fireRate = 0.3f; //added
    private float nextFire = 0; //added
    private bool isFacingRight = true; //added
    private int maxCrystals;
    private int crystals;

    public float moveForce = 150f;
    public float maxSpeed = 5f;
    public float jumpForce = 400f; 
    public Transform groundCheck;
    public GameObject fire;
    public Transform firePosition;
    public LayerMask groundIgnore;


    private void Awake()
    {
        maxCrystals = GameObject.FindGameObjectsWithTag("Pickup").Length;
    }
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        rend = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>(); 
    }

    void Update()
    {
        h = Input.GetAxis("Horizontal");

        // make player stop quicker
        if (Mathf.Abs(h) < 0.5)
            h = 0;

        // make sprite face correct direction
        if (h > 0 && !isFacingRight) //moving to right
            flip(); //updated
        else if (h < 0  && isFacingRight) //moving to left
            flip(); //updated

        Debug.DrawRay(groundCheck.position, new Vector2(0, 0.2f), Color.red, 1f);
        RaycastHit2D grounded = Physics2D.Raycast(groundCheck.position, Vector2.down, 0.2f, groundIgnore); //0.2f is distance

        if (grounded.collider != null)  //Player has landed
        {
            isGrounded = true;
            anim.SetBool("IsJumping", false);  
        }
        else
        {
            isGrounded = false;
            anim.SetBool("IsJumping", true);
        }

        // set player anim
        if (h != 0 && isGrounded)
            anim.SetBool("IsWalking", true);
        else
            anim.SetBool("IsWalking", false);

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            jump = true;
            isGrounded = false;
        }

        if (Input.GetButton("Fire1") && Time.time > nextFire)//added
        {
            nextFire = Time.time + fireRate;
            Instantiate(fire, firePosition);
        }
    }

    void FixedUpdate()
    {
        body.AddForce(Vector2.right * h * moveForce);

        // but limit how fast the player can move
        if (Mathf.Abs(body.velocity.x) > maxSpeed)
            body.velocity = new Vector2(Mathf.Sign(body.velocity.x) * maxSpeed, body.velocity.y);

        if (jump) 
        {
            body.AddForce(Vector2.up * jumpForce);
            jump = false;
        }

        // make player fall faster
        if (body.velocity.y >= 0)
            body.gravityScale = 1f;
        else
            body.gravityScale = 5f;

    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Pickup"))
            PickupCrystal(col.gameObject);
    }

    public Vector2 GetDirection()
    {
        if (isFacingRight) //added if/else here
            return Vector3.right;
        else
            return Vector3.left;
    }

    public void flip() //added this entire function
    {
        isFacingRight = !isFacingRight; // don't forget to update the flag!
        Vector3 theScale = transform.localScale;
        theScale.x = theScale.x * -1; //inverts the value
        transform.localScale = theScale;
    }
    void Die()
    {
        if (gameObject != null)
        {
            GameManager.instance.incrementRestarts();
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }

    private void PickupCrystal(GameObject crystal)
    {
        this.crystals++;
        if(this.crystals >= maxCrystals)
        {
            Die();
        }
        Destroy(crystal);
    }

    private void OnDrawGizmosSelected()
    {
        //RaycastHit2D grounded = Physics2D.Raycast(groundCheck.position, Vector2.down, 0.2f);
        Gizmos.DrawLine(groundCheck.position, Vector2.down * 0.2f);
    }
}
