﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //added


public class BoundaryController : MonoBehaviour
{
    #region Enumurations
    public enum BoundaryType
    {
        Enter, Exit
    }
    public enum ControllerType
    {
        Enemy,
        Scene
    }
    public enum CollisionType
    {
        Trigger,
        Collider
    }

    #endregion
    #region Fields
    public BoundaryType type;
    public ControllerType controllerType;
    public CollisionType collisionType;
    public string compareTag;
    #endregion
    #region OnEnter
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collisionType != CollisionType.Collider || type != BoundaryType.Enter)
            return;

        switch (controllerType)
        {
            case ControllerType.Enemy:
                if (collision.gameObject.CompareTag(compareTag))
                {
                    GameManager.instance.incrementRestarts();
                    SceneManager.LoadScene(0);
                }
                break;
            case ControllerType.Scene:
                if (collision.gameObject.CompareTag(compareTag))
                {
                    GameManager.instance.incrementRestarts();
                    SceneManager.LoadScene(0);
                }
                break;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collisionType != CollisionType.Trigger || type != BoundaryType.Enter)
            return;

        switch (controllerType)
        {
            case ControllerType.Enemy:
                if (collision.gameObject.CompareTag(compareTag))
                {
                    GameManager.instance.incrementRestarts();
                    SceneManager.LoadScene(0);
                }
                break;
            case ControllerType.Scene:
                if (collision.gameObject.CompareTag(compareTag))
                {
                    GameManager.instance.incrementRestarts();
                    SceneManager.LoadScene(0);
                }
                break;
        }
    }
    #endregion
    #region OnExit
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collisionType != CollisionType.Collider || type != BoundaryType.Exit)
            return;

        switch (controllerType)
        {
            case ControllerType.Enemy:
                if (collision.gameObject.CompareTag(compareTag))
                {
                    GameManager.instance.incrementRestarts();
                    SceneManager.LoadScene(0);
                }
                break;
            case ControllerType.Scene:
                if (collision.gameObject.CompareTag(compareTag))
                {
                    GameManager.instance.incrementRestarts();
                    SceneManager.LoadScene(0);
                }
                break;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collisionType != CollisionType.Trigger || type != BoundaryType.Exit)
            return;

        switch (controllerType)
        {
            case ControllerType.Enemy:
                if (collision.gameObject.CompareTag(compareTag))
                {
                    GameManager.instance.incrementRestarts();
                    SceneManager.LoadScene(0);
                }
                break;
            case ControllerType.Scene:
                if (collision.gameObject.CompareTag(compareTag))
                {
                    GameManager.instance.incrementRestarts();
                    SceneManager.LoadScene(0);
                }
                break;
        }
    }
    #endregion
}
