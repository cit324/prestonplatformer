﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftRight : MonoBehaviour
{
    public SpriteRenderer flip;
    public LayerMask ground;
    private int direction = 1; //starts moving to the right
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 1f, ground);
        if(hit.collider == null) //no ground beneath the dragon
        {
            direction = direction * -1; //switch directions
        }
        transform.position = Vector2.Lerp(transform.position, new Vector2(transform.position.x + 1 * direction, transform.position.y), Time.deltaTime);

        flip.flipX = direction == -1;
    }
}
