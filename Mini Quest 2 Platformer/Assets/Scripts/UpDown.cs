﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpDown : MonoBehaviour
{
    private int amt = 5;
    private int direction = 1; //positive initially
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("MoveObject");
    }

    // Update is called once per frame
    IEnumerator MoveObject()
    {
        while (true)
        {
            transform.Translate(new Vector2(0, 0.1f * direction));
            amt--;
            if (amt <= 0)
            {
                amt = 5;
                direction = direction * -1;
            }
            yield return new WaitForSeconds(0.2f); //will pause every half second
        }
    }

    private void Update()
    {
        
    }
}
